/*
  Copyright (c) 2010 - 2017, Nordic Semiconductor ASA
  All rights reserved.
  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.
  2. Redistributions in binary form, except as embedded into a Nordic
     Semiconductor ASA integrated circuit in a product or a software update for
     such product, must reproduce the above copyright notice, this list of
     conditions and the following disclaimer in the documentation and/or other
     materials provided with the distribution.
  3. Neither the name of Nordic Semiconductor ASA nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.
  4. This software, with or without modification, must only be used with a
     Nordic Semiconductor ASA integrated circuit.
  5. Any software provided in binary form under this license must not be reverse
     engineered, decompiled, modified and/or disassembled.
  THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
  OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

'use strict';

const {
  Adapter,
  Device,
  Property
} = require('gateway-addon');
 
var Noble = require('noble');
var tempB4 = 'temperature';
var humidB4 = 'humidity';
var self;

//console.log('Reading ELA RHT sensors!');

class ElaDevice extends Device {
  constructor(adapter, tag, pollInterval) {
    super(adapter, `${ElaDevice.name}-${tag.id}`);
    this.tag = tag;
    this['@context'] = 'https://iot.mozilla.org/schemas/';
    this['@type'] = ['TemperatureSensor', 'MultiLevelSensor'];
    this.name = `${tag.advertisement.localName}-${tag.id}`;
    this.description = `${tag.advertisement.localName}-${tag.id}`;
    this.pollinterval = (pollInterval || 30) * 1000;

    this.properties.set(
      'temperature',
      new Property(
        this,
        'temperature',
        {
          label: 'Temperature',
          '@type': 'TemperatureProperty',
          type: 'integer',
          unit: 'degree celsius',
          readOnly: true,
        },
        null
      )
    );

//  if(this.tag.advertisement.localName !== 'P T EN 800329') {
    if(!this.tag.advertisement.localName.toString('utf-8').includes('P T EN')) {
    this.properties.set(
      'humidity',
      new Property(
        this,
        'humidity',
        {
          label: 'Humidity',
          '@type': 'LevelProperty',
          type: 'integer',
          unit: 'percent',
          minimum: 0,
          maximum: 100,
          readOnly: true,
        },
        null
      )
    );
   }
    this.poll();

  }

/*  async startPolling(interval) {
    await this.updateData(this.tag);
//    Noble.stopScanning();
    this.timer = setInterval(() => {
      this.poll();
    }, interval * 1000);
}
*/

 async poll() {
     await this.updateData(this.tag);
     setTimeout(this.poll.bind(this), this.pollinterval);
  }

   async updateData(devID) {
    return new Promise((resolve, reject) => {
    var serviceData = devID.advertisement.serviceData;
     if (serviceData && serviceData.length) {
       var uuid;	
       for (var i in serviceData) {
        uuid = serviceData[i].uuid.toString('utf-8');
  //      console.log('UUID: ', uuid);
         if (uuid === "2a6e") {
	     if (JSON.stringify(serviceData[i].data.readUInt16LE(0)) & 0x8000) {
	        const [ signed ] = new Int16Array([serviceData[i].data.readUInt16LE(0)])
	        this.updateValue('temperature', (signed / 100));
	     } else {
	         this.updateValue('temperature', (serviceData[i].data.readUInt16LE(0) / 100));
             }   
         } else {
	    this.updateValue('humidity', serviceData[i].data.readUInt8(0));
  //          console.log('\t\t Humid: ' + serviceData[i].data.readUInt8(0));
         }
       }
	resolve();       
     } else {
	reject();
     }
    });
   }

   async devDiscover() {
    return new Promise((resolve, reject) => {
      Noble.on('discover', function(peripheral) {
	   if (peripheral.id === 'f78c077ecab2' || peripheral.id === 'e0514538b920' || peripheral.advertisement.localName === 'P T EN 800329' ) {
//	       console.log(`Discovered new ElaDevice ${peripheral.id}`);
	       resolve(peripheral);
           }
      });
    });
  }

  updateValue(name, value) {
//    console.log(`Set ${name} to ${value}`);
    const property = this.properties.get(name);
    property.setCachedValue(value);
    this.notifyPropertyChanged(property);
  }

  async sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });
  }

}

class ElaAdapter extends Adapter {
  constructor(addonManager, manifest) {
    super(addonManager, ElaAdapter.name, manifest.name);
    const pollInterval = manifest.moziot.config.pollInterval;
    addonManager.addAdapter(this);
    this.knownDevices = new Set();
    this.pollinterval = pollInterval;
    self = this;    
    Noble.startScanning([], true);
    this.DiscoverandAddDev(pollInterval);
  }

   async DiscoverandAddDev(pollInterval) {
     const peripheral = await this.discover(pollInterval);
   }

   async discover(pollInterval) {
    return new Promise((resolve, reject) => {
      Noble.on('discover', function(peripheral) {
      if(peripheral.advertisement.localName) {
//	   if (peripheral.id === 'f78c077ecab2' || peripheral.id === 'e0514538b920' || peripheral.advertisement.localName === 'P T EN 800329' 
	   if (peripheral.advertisement.localName.toString('utf-8').includes('P RHT') || peripheral.advertisement.localName.toString('utf-8').includes('P T EN')) {
//	       console.log(`Discovered new ElaDevice ${peripheral.id}`);
	     const knownDevice = self.knownDevices.has(peripheral);
//	     console.log('Known Device value:', knownDevice);
	      if (!knownDevice) {
		console.log(`Detected new ElaDevice ${peripheral.advertisement.localName}-${peripheral.id}`);
		const device = new ElaDevice(self, peripheral, self.pollInterval);
		self.knownDevices.add(peripheral);
		self.handleDeviceAdded(device);
	      }
           }
	 }
      });
    });
  }
}


module.exports = ElaAdapter;
