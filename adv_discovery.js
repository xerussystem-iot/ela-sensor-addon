var noble = require('noble');

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    console.log('Starting scan');
    noble.startScanning([], true);
  } else {
    console.log('Stopping scan');
    noble.stopScanning();
  }
});

noble.on('discover', function(peripheral) {
 if (peripheral.advertisement.localName) {
   if (peripheral.advertisement.localName.toString('utf-8').includes('P RHT') || peripheral.advertisement.localName.toString('utf-8').includes('P T EN')) {
console.log('ELA peripheral device:' +peripheral);
/*  console.log('peripheral discovered (' + peripheral.id +
              ' with address <' + peripheral.address +  ', ' + peripheral.addressType + '>,' +
              ' connectable ' + peripheral.connectable + ',' +
              ' RSSI ' + peripheral.rssi + ':');
  console.log('\thello my local name is:');
  console.log('\t\t' + peripheral.advertisement.localName.toString('utf-8').includes('BT'));
  console.log('\tcan I interest you in any of the following advertised services:');
  console.log('\t\t' + JSON.stringify(peripheral.advertisement.serviceUuids));
*/
  var serviceData = peripheral.advertisement.serviceData;
  if (serviceData && serviceData.length) {
    console.log('\there is my service data:');
    for (var i in serviceData) {
        if(serviceData[i].uuid.toString('utf-8') === "2a6e") {
	  const [ signed ] = new Int16Array([serviceData[i].data.readUInt16LE(0)]);
          console.log('\t\t' + JSON.stringify(serviceData[i].uuid) + ': ' + JSON.stringify(serviceData[i].data.toString('hex')));
          console.log('\t\t' + `2s complement data ${serviceData[i].uuid}` + ': ' + signed / 100 );
//          console.log('\t\t' + `2s complement data ${serviceData[i].uuid}` + ': ' + -(((~notOfval) & ((1 << 16) - 1)) + 1) / 100 ); //(((~notOfval) & ((1 << 8) - 1)) ) / 100 );
      }
    }
  }
  if (peripheral.advertisement.manufacturerData) {
    console.log('\there is my manufacturer data:');
    console.log('\t\t' + JSON.stringify(peripheral.advertisement.manufacturerData.toString('hex')));
  }
  if (peripheral.advertisement.txPowerLevel !== undefined) {
    console.log('\tmy TX power level is:');
    console.log('\t\t' + peripheral.advertisement.txPowerLevel);
  }
 }
}

  console.log();
});
