
'use strict';

// eslint-disable-next-line max-len
const ElaAdapter = require('./ela');

module.exports =
    // eslint-disable-next-line max-len
    (addonManager, manifest) => new ElaAdapter(addonManager, manifest);
